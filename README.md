# Probing task for new devs

## General notes

- Due date: 14/11/2018
- Project key to use for version control: PRT
- Reviewers: Isaac Montes, Victor Avelar
- Questions: deltatuts@gmail.com | imontes2893@gmail.com

## User story

As a traveler, I will like to have a simple app in which I can keep track of my experiences around the world, being able to share my view of different cultures and costumes is something I want to accomplish in the near future.

I love to take pictures, and I will like to share that with my followers, I will also like to allow them to drop comments on my stories to take feedback and improve the way I tell the world how aazing is to travel.

## Technical note

In order to complete the request from our user, we need to implement a blogging platform that empowers him to fulfill his dreams and expectations

This requires both frontend and backend, so I will advice you to work in parallel to finish on time, the due date is of 1 week.

## Issues 

Please go to the issue tracking tab to see the tasks we have prepared.

## Acceptance criteria

- All the tasks are finished and reviewed
- The system works as expected
- We are able to publish notes.
- Users are able to comment the posts.
- The posts are shareable in social media platforms with card display.
- The design is responsive.

## Tooling

To implement this task please use the following tools.

1. Laravel framework v5.7 [Laravel website](https://www.laravel.com) for the backend functionality
2. Bootstrap v4 [Bootstrap](https://getbootstrap.com/) for the frontend implementation
3. Laravel mix v2 for assets processing.
4. Git for version control

## Development flow

This is the development workflow we spec you to follow during this probing.

1. Take an issue from the issue tracker and assign it to you.
2. Create a branch based on this issue, for example:
	- I take the issue `PRT-1 Create a database table for users` and assign it to me
	- I create a branch following the pattern: `feature/PRT-1-create-a-database-table-for-users`
3. Implement the task in this branch with relevant commits, for example:
	- I create the schema for the table
	- I add the changes to Git tracking
	- I commit those changes with the message `PRT-1 - Created schema for users table`
	- I reflect the schema in the table Model class
	- I commit this changes with the message `PRT-1 - Implemented fillable fields in user models`
4. Push your commits
5. When the implementation is complete, please create a pull request
6. Add the Team Leads as reviewers
7. Move to the next task